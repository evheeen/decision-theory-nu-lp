import numpy
from prettytable import PrettyTable


def getFile():
    text = []
    with open("file1", "r") as file:
        lines = file.readlines()
        for i in lines:
            text.append(i.replace('\n', ''))
    for i in range(len(text)):
        text[i] = text[i].split(" ")
    return text


def kondorse(array):
    ab = array[0] + array[1] + array[4]
    ba = array[2] + array[3] + array[5]
    ac = array[0] + array[1] + array[2]
    ca = array[3] + array[4] + array[5]
    bc = array[0] + array[2] + array[3]
    cb = array[1] + array[4] + array[5]
    
    print("\tCompare:")
    print("\t\tA>B:", ab)
    print("\t\tB>A:", ba)
    print("\t\tA>C:", ac)
    print("\t\tC>A:", ca)
    print("\t\tB>C:", bc)
    print("\t\tC>B:", cb)
    
    first = numpy.max([ab, ba])
    second = numpy.max([ac, ca])
    third = numpy.max([bc, cb])
    
    print("\tSo...")
    print("\t\tBetween A and B wins A") if first == ab else print("\t\tBetween A and B wins B")
    print("\t\tBetween A and C wins A") if second == ac else print("\t\tBetween A and C wins C")
    print("\t\tBetween B and C wins B") if third == bc else print("\t\tBetween B and C wins C")
    
    result = numpy.max([first, second, third])
    
    print("\tAnd...")
    if result == ab:
        print("\t\tWins A candidate")
        return "A"
    elif result == ba:
        print("\t\tWins B candidate")
        return "B"
    elif result == ac:
        print("\t\tWins A candidate")
        return "A"
    elif result == ca:
        print("\t\tWins C candidate")
        return "C"
    elif result == bc:
        print("\t\tWins B candidate")
        return "B"
    elif result == cb:
        print("\t\tWins C candidate")
        return "C"
    

def borda(array):
    a = array[0] * 2 + array[1] * 2 + array[2] + array[4]
    b = array[2] * 2 + array[3] * 2 + array[0] + array[5]
    c = array[4] * 2 + array[5] * 2 + array[1] + array[3]
    
    print("\tCandidate marks:")
    print("\t\tA:", a)
    print("\t\tB:", b)
    print("\t\tC:", c)
    
    result = numpy.max([a, b, c])
    
    if result == a:
        print("\tWins A candidate")
        return "A"
    elif result == b:
        print("\tWins B candidate")
        return "B"
    elif result == c:
        return "C"


if __name__ == '__main__':
    array = getFile()
    array = [[int(j) if '.' not in j else float(j) for j in i] for i in array]

    table = PrettyTable(['Number of voters', 'Benefits'])
    table.add_row([24, 'A->B->C'])
    table.add_row([23, 'A->C->B'])
    table.add_row([26, 'B->A->C'])
    table.add_row([6, 'B->C->A'])
    table.add_row([12, 'C->A->B'])
    table.add_row([7, 'C->B->A'])
    
    print(table)
    print("\nBy Kondorse:")
    answerFirst = kondorse(array[0])
    print("\nBy Borda:")
    answerSecond = borda(array[0])
    
    print("\nThe results are equal") if answerFirst == answerSecond else print("The results are not equal")
